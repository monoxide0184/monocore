package monoxide.core.libraries;

import java.net.MalformedURLException;
import java.net.URL;

class MavenSpec {
	private final String group;
	private final String artifact;
	private final String version;
	private final URL repository;

	public MavenSpec(URL repository, String specification) {
		String[] parts = specification.split(":");

		this.repository = repository;
		this.group = parts[0];
		this.artifact = parts[1];
		this.version = parts[2];
	}

	public URL getDownloadURL() throws MalformedURLException {
		return new URL(repository, getRelativePath());
	}

	public String getRelativePath() {
		return String.format("%s/%s/%s/%s-%s.jar", group.replace('.', '/'), artifact, version, artifact, version);
	}
}
