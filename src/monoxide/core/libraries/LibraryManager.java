package monoxide.core.libraries;

import monoxide.core.MonoCore;
import net.minecraft.launchwrapper.LaunchClassLoader;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;

public final class LibraryManager {
	private LibraryManager() {}

	public static void injectLibraries(LaunchClassLoader classLoader) {
		File modsFolder = new File(MonoCore.getMcLocation(), "mods");
		File libraryFolder = new File(MonoCore.getMcLocation(), "libraries");

		MonoCore.getLogger().info("Searching for extra required libraries in %s.", modsFolder);

		File[] jars = modsFolder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File file, String s) {
				return s.endsWith(".jar");
			}
		});

		for (File jar : jars) {
			Properties props;
			try {
				URI propsFile = new URI("jar:" + jar.toURI().toString() + "!/MonoCore.properties");
				props = new Properties();
				props.load(propsFile.toURL().openStream());
			} catch (URISyntaxException e) {
				MonoCore.getLogger().log(Level.INFO, e, "Invalid URI syntax");
				continue;
			} catch (IOException e) {
				continue;
			}

			MonoCore.getLogger().info("Found properties file in: %s", jar.getAbsolutePath());
			URL source;
			try {
				source = new URL(props.getProperty("library.source", "http://repo.maven.apache.org/maven2/"));
			} catch (MalformedURLException e) {
				MonoCore.getLogger().warning("Invalid library source specified in this mod, ignoring mod.");
				continue;
			}

			for (Object key : props.keySet()) {
				String keyString = (String) key;
				if (!keyString.startsWith("libraries.")) { continue; }

				MavenSpec maven = new MavenSpec(source, props.getProperty(keyString));
				File library = new File(libraryFolder, maven.getRelativePath());

				if (library.exists()) {
					addFileToClasspath(classLoader, library);
					continue;
				}

				MonoCore.getLogger().info("Unable to find library %s, downloading now.", keyString.substring(10));

				try {
					library.getParentFile().mkdirs();
					InputStream input = maven.getDownloadURL().openStream();
					OutputStream output = new FileOutputStream(library);

					byte data[] = new byte[8192];
					int count;
					while ((count = input.read(data, 0, 8192)) != -1)
					{
						output.write(data, 0, count);
					}

					addFileToClasspath(classLoader, library);
				} catch (Throwable e) {
					MonoCore.getLogger().log(Level.SEVERE, e, "Unable to download library. This isn't likely to end well.");
				}
			}
		}
	}

	private static void addFileToClasspath(LaunchClassLoader classLoader, File library) {
		try {
			classLoader.addURL(library.toURI().toURL());
		} catch (MalformedURLException e) {
			MonoCore.getLogger().log(Level.SEVERE, e, "Unable to add required library to classpath. This isn't likely to end well.");
		}
	}
}
