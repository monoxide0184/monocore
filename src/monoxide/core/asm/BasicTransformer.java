package monoxide.core.asm;

import com.google.common.collect.Lists;
import monoxide.core.MonoCore;
import monoxide.core.logging.MonoLog;
import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import java.util.Collections;
import java.util.List;

public abstract class BasicTransformer implements IClassTransformer {
	protected MonoLog logger = MonoCore.getLogger();
	private final List<String> classes = Lists.newArrayList();

	protected void addClass(String... names) {
		Collections.addAll(classes, names);
	}

	@Override
	public final byte[] transform(String name, String transformedName, byte[] bytes) {
		if (!classes.contains(transformedName)) { return bytes; }

		logger.info("Inserting hooks into %s (%s) for %s", name, transformedName, this.getClass().getCanonicalName());
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(bytes);
		classReader.accept(classNode, 0);

		transform(classNode);

		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		classNode.accept(writer);
		return writer.toByteArray();

	}

	public abstract void transform(ClassNode classNode);
}
