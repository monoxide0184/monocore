package monoxide.core.logging;

import cpw.mods.fml.common.FMLLog;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MonoLog {
	private Logger logger;

	public MonoLog(String loggerName) {
		logger = Logger.getLogger(loggerName);
		logger.setParent(FMLLog.getLogger());
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void setLoggerParent(Logger logger) {
		this.logger.setParent(logger);
	}

	public void log(Level logLevel, String message, Object... params) {
		boolean lowLogLevel = logLevel == Level.FINE || logLevel == Level.FINER || logLevel == Level.FINEST;
		Logger old = logger.getParent();
		if (lowLogLevel) {
			logger.setParent(FMLLog.getLogger());
		}
		logger.log(logLevel, String.format(message, params));
		if (lowLogLevel) {
			logger.setParent(old);
		}
	}

	public void log(Level logLevel, Throwable e, String message, Object... params) {
		Logger old = logger.getParent();
		logger.setParent(FMLLog.getLogger());
		logger.log(logLevel, String.format(message, params), e);
		logger.setParent(old);
	}

	public void error(String message, Object... params) {
		log(Level.SEVERE, message, params);
	}

	public void warning(String message, Object... params) {
		log(Level.WARNING, message, params);
	}

	public void info(String message, Object... params) {
		log(Level.INFO, message, params);
	}

	public void config(String message, Object... params) {
		log(Level.CONFIG, message, params);
	}

	public void fine(String message, Object... params) {
		log(Level.FINE, message, params);
	}

	public void finer(String message, Object... params) {
		log(Level.FINER, message, params);
	}

	public void finest(String message, Object... params) {
		log(Level.FINEST, message, params);
	}

	public void dumpStackTrace(String message, Object... params) {
		String output = String.format(message, params) + "\n";

		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		for (int i = 2; i < stackTrace.length; i++) {
			output += "  in " + stackTrace[i].toString() + "\n";
		}

		info(output.trim());
	}
}
